package br.edu.fatecscj;

public class CalculadoraAluguel {

    private double aluguelNominal;

    public CalculadoraAluguel(double aluguelNominal) {
        this.setAluguelNominal(aluguelNominal);
    }

    public double getAluguelNominal() {
        return aluguelNominal;
    }

    public void setAluguelNominal(double aluguelNominal) {
        this.aluguelNominal = aluguelNominal;
    }

    public double calculoAluguel(int diaMes) {

        if (diaMes >= 1 && diaMes <= 5)
            return this.getAluguelNominal() * 0.90;
        else if (diaMes >= 1 && diaMes <= 10)
            return diaMes == 6 ? this.getAluguelNominal() * 0.95D + 0.02D : this.getAluguelNominal() * 0.95D;
        else if (diaMes >= 1 && diaMes < 15)
            return this.getAluguelNominal();
        else
            return diaMes >= 1 && diaMes <= 30 ? this.getAluguelNominal() * 1.02D + this.getAluguelNominal() * 5.0E-4D * (double) (diaMes - 15) : -1.0D;
    }
}
