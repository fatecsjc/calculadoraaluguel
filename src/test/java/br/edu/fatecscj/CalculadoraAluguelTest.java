package br.edu.fatecscj;

import org.junit.jupiter.api.Test;

import static org.junit.jupiter.api.Assertions.assertEquals;

public class CalculadoraAluguelTest {

    private CalculadoraAluguel calculadoraAluguel;

    @Test
        // testShouldPass
    void testaCalcAluguelDiaMesIgualZero() {

        calculadoraAluguel = new CalculadoraAluguel(500.00);
        assertEquals(-1.0, calculadoraAluguel.calculoAluguel(0), 0.0f);
    }

    @Test // testShouldPass
    public void testaCalcAluguelDiaMesIgualUm() {

        calculadoraAluguel = new CalculadoraAluguel(500.00);
        double valor = calculadoraAluguel.calculoAluguel(1);
        assertEquals(calculadoraAluguel.getAluguelNominal() * 0.9D, valor, 0.0f);
    }

    @Test // testShouldPass
    public void testaCalcAluguelDiaMesIgualCinco() {

        calculadoraAluguel = new CalculadoraAluguel(500.00);
        double valor = calculadoraAluguel.calculoAluguel(5);
        assertEquals(calculadoraAluguel.getAluguelNominal() * 0.9D, valor, 0.0f);
    }

    @Test // testShouldPass
    public void testaCalcAluguelDiaMesIgualSeis() {

        calculadoraAluguel = new CalculadoraAluguel(500.00);
        double valor = calculadoraAluguel.calculoAluguel(6);
        assertEquals(calculadoraAluguel.getAluguelNominal() * 0.95 + 0.02, valor, 0.0f);
    }

    @Test // testShouldPass
    public void testaCalcAluguelDiaMesIgualSete() {

        calculadoraAluguel = new CalculadoraAluguel(500.00);
        double valor = calculadoraAluguel.calculoAluguel(7);
        assertEquals(calculadoraAluguel.getAluguelNominal() * 0.95, valor, 0.0f);
    }

    @Test // testShouldPass
    public void testaCalcAluguelDiaMesIgualDez() {

        calculadoraAluguel = new CalculadoraAluguel(500.00);
        double valor = calculadoraAluguel.calculoAluguel(10);
        assertEquals(calculadoraAluguel.getAluguelNominal() * 0.95, valor, 0.0f);
    }

    @Test // testShouldPass
    public void testaCalcAluguelDiaMesIgualOnze() {

        calculadoraAluguel = new CalculadoraAluguel(500.00);
        double valor = calculadoraAluguel.calculoAluguel(11);
        assertEquals(calculadoraAluguel.getAluguelNominal(), valor, 0.0f);
    }

    @Test // testShouldPass
    public void testaCalcAluguelDiaMesIgualQuatorze() {

        calculadoraAluguel = new CalculadoraAluguel(500.00);
        double valor = calculadoraAluguel.calculoAluguel(14);
        assertEquals(calculadoraAluguel.getAluguelNominal(), valor, 0.0f);
    }

    @Test // testShouldPass
    public void testaCalcAluguelDiaMesIgualQuinze() {

        calculadoraAluguel = new CalculadoraAluguel(500.00);
        double valor = calculadoraAluguel.calculoAluguel(15);
        assertEquals(calculadoraAluguel.getAluguelNominal() * 1.02
                + calculadoraAluguel.getAluguelNominal() * 5.0E-4 * (double) (15 - 15), valor, 0.0f);
    }

    @Test // testShouldPass
    public void testaCalcAluguelDiaMesIgualTrinta() {

        calculadoraAluguel = new CalculadoraAluguel(500.00);
        double valor = calculadoraAluguel.calculoAluguel(30);
        assertEquals(calculadoraAluguel.getAluguelNominal() * 1.02
                + calculadoraAluguel.getAluguelNominal() * 5.0E-4 * (double) (30 - 15), valor, 0.0f);
    }

    @Test // testShouldPass
    public void testaCalcAluguelDiaMesMaiorQueTrinta() {

        calculadoraAluguel = new CalculadoraAluguel(500.00);
        double valor = calculadoraAluguel.calculoAluguel(31);
        assertEquals(-1.0, valor, 0.0f);
    }
}
